<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

use App\Model\Admin\Guru;
use App\Model\Admin\User;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Guru::paginate(10);
        return view('adminlte.guru.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('adminlte.guru.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
        'nip' => 'unique:guru|required',
        'nama' => 'required',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required',
        'alamat' => 'required',
        'jenis_kelamin' => 'required',
        'no_telp' => 'required|unique:guru',
        'mapel_id' => 'required|unique:guru',
        'email' => 'required|unique:users',
        'password' => 'required'
      ]);

        $data = [
          'name' => $request->nama,
          'email' => $request->email,
          'password' => bcrypt($request->password),
          'role' => 'guru'
        ];

        $user = User::create($data);

        $guru = $request->all();

        $guru['user_id'] = $user->id;

        Guru::create($guru);

        return redirect()->route('guru.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $guru = Guru::find($id);
        return view('adminlte.guru.edit',compact('guru'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = [
          'nama' => 'required',
          'tempat_lahir' => 'required',
          'tgl_lahir' => 'required',
          'alamat' => 'required',
          'jenis_kelamin' => 'required',
        ];

        $guru = Guru::find($id);

        if ($guru->nip != $request->nip) {
          $rules['nip'] = 'unique:guru|required';
        }
        elseif ($guru->no_telp != $request->no_telp) {
          $rules['no_telp'] = 'required|unique:guru';
        }
        elseif ($guru->mapel_id != $request->mapel_id) {
          $rules['mapel_id'] = 'required|unique:guru';
        }
        elseif ($guru->email != $request->email) {
          $rules['email'] = 'required|unique:users';
        }

        $this->validate($request,$rules);

        $data = $request->except('password');

        if ($request->password != '') {
          $data['password'] = $request->password;
        }

        $guru->update($data);

        // if ($request->password != '') {
        //
        // }

        return redirect()->route('guru.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Guru::find($id)->delete();

        return redirect()->route('guru.index');
    }
}
