<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

use App\Model\Admin\Siswa;
use App\Model\Admin\User;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Siswa::paginate(10);
        return view('adminlte.siswa.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminlte.siswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
        'nis' => 'unique:siswa|required',
        'nama' => 'required',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required',
        'alamat' => 'required',
        'jenis_kelamin' => 'required',
        'no_telp' => 'required|unique:siswa',
        'angkatan' => 'required',
        'kelas_id' => 'required',
        'email' => 'required|unique:users',
        'password' => 'required'
      ]);

        $data = [
          'name' => $request->nama,
          'email' => $request->email,
          'password' => Hash::make($request->password),
          'role' => 'siswa'
        ];

        $user = User::create($data);

        $siswa = $request->all();

        $siswa['user_id'] = $user->id;

        Siswa::create($siswa);

        return redirect()->route('siswa.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $siswa = Siswa::find($id);
      return view('adminlte.siswa.edit',compact('siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $rules = [
        'nama' => 'required',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required',
        'alamat' => 'required',
        'jenis_kelamin' => 'required',
        'angkatan' => 'required',
        'kelas_id' => 'required',
        'password' => 'required'
      ];

      $siswa = Siswa::find($id);

      if ($siswa->nis != $request->nis) {
        $rules['nis'] = 'unique:siswa|required';
      }
      elseif ($siswa->no_telp != $request->no_telp) {
        $rules['no_telp'] = 'required|unique:siswa';
      }
      elseif ($siswa->email != $request->email) {
        $rules['email'] = 'required|unique:users';
      }

      $this->validate($request,$rules);

      $data = $request->except('password');

      if ($request->password != '') {
        $data['password'] = $request->password;
      }

      $siswa->update($request->all());

      return redirect()->route('siswa.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $siswa = Siswa::find($id);
        $siswa->delete();

        return redirect()->route('siswa.index');
    }
}
