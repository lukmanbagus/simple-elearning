<?php

namespace App\Http\Controllers\Guru;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Guru\Diskusi;
use App\Model\Guru\DiskusiContent;

class DiskusiController extends Controller
{
  public function index()
  {
    $data = \Auth::user()->guru->mapel->materi;
    return view('guru.diskusi.index',compact('data'));
  }

  public function create($id)
  {
    Diskusi::create([
      'materi_id' => $id
    ]);

    return redirect()->route('materi.index');
  }

  public function open($id)
  {
    $data = Diskusi::find($id);
    return view('guru.diskusi.open',compact('data'));
  }

  public function store(Request $request)
  {
    DiskusiContent::create($request->all());
    return redirect()->route('diskusi.guru.open',$request->diskusi_id);
  }


}
