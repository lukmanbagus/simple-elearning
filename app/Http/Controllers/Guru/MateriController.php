<?php

namespace App\Http\Controllers\Guru;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Model\Guru\Materi;
use App\Model\Admin\Guru;
use App\Model\Admin\Mapel;
use App\Model\Admin\Siswa;
use App\Model\Guru\Jawaban;
use App\Model\Guru\Nilai;
use App\Model\Guru\Soal;

class MateriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = \Auth::user()->guru->mapel->materi;
        // return response()->json($data);
        return view('guru.materi.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('guru.materi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $guru = Guru::where('email',\Auth::user()->email)->with('mapel')->first();

        $data = [
          'nama_materi' => $request->nama_materi,
          'content' => $request->content,
          'mapel_id' => $guru->mapel->id
        ];

        if ($request->hasFile('file')) {
          $data['file'] = $this->saveFile($request->file);
        }

        // return json_encode($request->all());

        $materi = Materi::create($data);

        $materi->kelas()->sync($request->kelas);

        return redirect()->route('materi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $materi = Materi::find($id);
        return view('guru.materi.edit',compact('materi'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $guru = Guru::where('email',\Auth::user()->email)->with('mapel')->first();

        $data = [
          'nama_materi' => $request->nama_materi,
          'content' => $request->content,
          'mapel_id' => $guru->mapel->id
        ];

        if ($request->hasFile('file')) {
          $data['file'] = $this->saveFile($request->file);
        }

        // return json_encode($request->all());

        $materi = Materi::find($id);
        $materi->update($data);

        $materi->kelas()->sync($request->kelas);

        return redirect()->route('materi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $materi = Materi::find($id);
        $materi->kelas()->detach();
        $materi->jawaban()->delete();
        $materi->delete();

        return redirect()->route('materi.index');
    }

    protected function saveFile(UploadedFile $file)
    {
      $fileName = str_random(40) . '.' . $file->guessClientExtension();
      $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'files';
      $file->move($destinationPath, $fileName);
      return $fileName;
    }

    public function jawaban()
    {
      $data = \Auth::user()->guru->mapel->materi;
      return view('guru.jawaban.index',compact('data'));
    }

    public function listsiswa($id)
    {
      $data = Materi::find($id)->kelas;
      $materi = ['materi_id' => $id];
      return view('guru.nilai.index',compact('data'),$materi);
    }

    public function inputnilai($materi_id,$siswa_id)
    {
      $siswa = Siswa::find($siswa_id);
      // return response()->json($siswa);
      return view('guru.nilai.input',compact('siswa'))->with('materi_id',$materi_id);
    }

    public function editnilai($materi_id,$siswa_id)
    {
      $siswa = Siswa::find($siswa_id);
      $nilai = Nilai::where('materi_id',$materi_id)->where('siswa_id',$siswa_id)->first();
      // return response()->json($nilai);
      return view('guru.nilai.edit',compact('siswa'),compact('nilai'))->with('materi_id',$materi_id);
    }

    public function storenilai(Request $request)
    {
      Nilai::create($request->all());
      return redirect()->route('materi.listsiswa',$request->materi_id);
    }

    public function soalLatihan($id)
    {
      $soals = Soal::where('materi_id',$id)->get();
      return view('guru.soal.index',compact('soals'))->with(['id_materi' => $id]);
    }

    public function createSoal($id)
    {
      $materi = Materi::find($id);
      return view('guru.soal.create',compact('materi'));
    }

    public function storeSoal(Request $request)
    {
      Soal::create($request->all());
      return redirect()->route('materi.soal-latihan',$request->materi_id);
    }

    public function updateSoal(Request $request,$id)
    {
      $soal = Soal::find($id);

      $soal->update($request->all());

      return redirect()->route('materi.soal-latihan',$request->materi_id);
    }

    public function editSoal($id)
    {
      $soal = Soal::find($id);
      $materi = Materi::find($soal->materi_id);
      return view('guru.soal.edit',compact('soal'))->with(['materi' => $materi]);
    }

    public function soalDestroy($id)
    {
      $soal = Soal::find($id);
      $materi_id = $soal->materi_id;
      $soal->delete();
      return redirect()->route('materi.soal-latihan',$materi_id);
    }
}
