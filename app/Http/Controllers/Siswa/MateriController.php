<?php

namespace App\Http\Controllers\Siswa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Model\Guru\Materi;
use App\Model\Admin\Guru;
use App\Model\Admin\Mapel;
use App\Model\Guru\Jawaban;
use App\Model\Guru\Nilai;
use App\Model\Admin\Kelas;
use App\Model\Admin\Siswa;
use App\Model\Guru\Soal;
use App\Model\Siswa\JawabanSiswa;
use App\Model\Siswa\HasilJawabanSiswa;
use App\User;

class MateriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kelas = Kelas::find(\Auth::user()->siswa->kelas_id);
        return view('siswa.materi.index',compact('kelas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('siswa.materi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $materi = Materi::find($id);
        return view('siswa.materi.edit',compact('materi'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $guru = Guru::where('email',\Auth::user()->email)->with('mapel')->first();

        $data = [
          'nama_materi' => $request->nama_materi,
          'content' => $request->content,
          'mapel_id' => $guru->mapel->id
        ];

        if ($request->hasFile('file')) {
          $data['file'] = $this->saveFile($request->file);
        }

        // return json_encode($request->all());

        $materi = Materi::find($id);
        $materi->update($data);

        $materi->kelas()->sync($request->kelas);

        return redirect()->route('materi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function saveFile(UploadedFile $file)
    {
      $fileName = str_random(40) . '.' . $file->guessClientExtension();
      $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'files';
      $file->move($destinationPath, $fileName);
      return $fileName;
    }

    public function tugas($id)
    {
      $materi = Materi::find($id);
      return view('siswa.materi.tugas',compact('materi'));
    }

    public function uploadtugas(Request $request)
    {
      $data = [
        'materi_id' => $request->materi_id,
        'user_id' => \Auth::user()->id,
        'file' => $this->saveFile($request->file)
      ];

      Jawaban::create($data);

      \Session::flash('success','File tugas berhasil di upload');

      return redirect()->route('materiku');
    }

    public function masuk($id)
    {
      $materi = Materi::find($id);
      return view('siswa.materi.masuk',compact('materi'));
    }

    public function filetugas()
    {
      $tugas = Jawaban::where('user_id',\Auth::user()->id)->get();
      return view('siswa.file.tugas',compact('tugas'));
    }

    public function nilai()
    {
      $data = Nilai::where('siswa_id',\Auth::user()->siswa->id)->get();
      return view('siswa.nilai.index',compact('data'));
    }

    public function soalLatihan($id)
    {
      $soals = Soal::where('materi_id',$id)->get();
      return view('siswa.soal.index',compact('soals'))->with(['id_materi' => $id]);
    }

    public function jawabSoalLatihan(Request $request,$id)
    {
      $materi = Materi::find($id);

      $jawaban = $request->except('_token');

      // return response($materi->soal);

      $nilai = [];
      foreach ($materi->soal as $soal) {
        if ($soal->jawaban == $jawaban[$soal->id]) {
          $nilai[] = 1;
        }

        // Masukan Jawaban Ke Database
        JawabanSiswa::create([
          'soal_id' => $soal->id,
          'siswa_id' => \Auth::user()->siswa->id,
          'jawaban' => $jawaban[$soal->id]
        ]);
      }

      $jumlahnilai = array_sum($nilai);
      $nilaitotal = ($jumlahnilai/$materi->soal->count())*10;

      HasilJawabanSiswa::create([
        'materi_id' => $id,
        'siswa_id' => \Auth::user()->siswa->id,
        'nilai' => $nilaitotal
      ]);

      return redirect()->route('materiku');
    }

}
