<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    //
    protected $table = 'guru';
    protected $fillable = [
      'nip',
      'nama',
      'tempat_lahir',
      'tgl_lahir',
      'alamat',
      'no_telp',
      'mapel_id',
      'jenis_kelamin',
      'email',
      'user_id'
    ];

    public function mapel()
    {
      return $this->belongsTo('App\Model\Admin\Mapel');
    }

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    // public function materi()
    // {
    //   return $this->hasManyThrough('App\Model\Guru\Materi','App\Model\Admin\Mapel');
    // }
}
