<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    //
    protected $table = 'kelas';
    protected $fillable = ['kelas','jurusan'];

    public function siswa()
    {
      return $this->hasMany('App\Model\Admin\Siswa');
    }

    public function materi()
    {
      return $this->belongsToMany('App\Model\Guru\Materi','materi_kelas','kelas_id','materi_id');
    }
}
