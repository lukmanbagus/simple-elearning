<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    //

    protected $table = 'mapel';
    protected $fillable = [
      'nama_mapel', 'keterangan'
    ];

    public function guru()
    {
      return $this->hasOne('App\Model\Admin\Guru');
    }

    public function materi()
    {
      return $this->hasMany('App\Model\Guru\Materi');
    }
}
