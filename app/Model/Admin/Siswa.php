<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    //
    protected $table = 'siswa';
    protected $fillable = [
      'nis',
      'nama',
      'tempat_lahir',
      'tgl_lahir',
      'alamat',
      'kelas_id',
      'no_telp',
      'angkatan',
      'jenis_kelamin',
      'email',
      'user_id',
    ];

    public function kelas()
    {
      return $this->belongsTo('App\Model\Admin\Kelas');
    }

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function nilai()
    {
      return $this->hasMany('App\Model\Guru\Nilai');
    }

    public function hasil()
    {
      return $this->hasMany('App\Model\Siswa\HasilJawabanSiswa');
    }
}
