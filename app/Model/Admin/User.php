<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //

    protected $fillable = ['name','email','role','password'];
}
