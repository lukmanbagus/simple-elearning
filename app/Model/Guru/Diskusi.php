<?php

namespace App\Model\Guru;

use Illuminate\Database\Eloquent\Model;

class Diskusi extends Model
{
    //
    protected $table = 'diskusi';
    protected $fillable = [
      'materi_id',
      'user_id',
      'content'
    ];

    public function materi()
    {
      return $this->belongsTo('App\Model\Guru\Materi');
    }

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function diskusi_content()
    {
      return $this->hasMany('App\Model\Guru\DiskusiContent');
    }
}
