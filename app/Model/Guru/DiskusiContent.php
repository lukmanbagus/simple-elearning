<?php

namespace App\Model\Guru;

use Illuminate\Database\Eloquent\Model;

class DiskusiContent extends Model
{
    //
    protected $table = 'diskusi_content';
    protected $fillable = [
      'diskusi_id',
      'user_id',
      'content'
    ];

    public function diskusi()
    {
      return $this->belongsTo('App\Model\Guru\Diskusi');
    }

    public function user()
    {
      return $this->belongsTo('App\User');
    }
}
