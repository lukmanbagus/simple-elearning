<?php

namespace App\Model\Guru;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    //
    protected $fillable = ['file','materi_id','user_id'];

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function materi()
    {
      return $this->belongsTo('App\Model\Guru\Materi');
    }
}
