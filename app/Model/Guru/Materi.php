<?php

namespace App\Model\Guru;

use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    //
    protected $table = 'materi';
    protected $fillable = [
      'nama_materi',
      'content',
      'file',
      'mapel_id'
    ];

    public function kelas()
    {
      return $this->belongsToMany('App\Model\Admin\Kelas','materi_kelas','materi_id','kelas_id');
    }

    public function mapel()
    {
      return $this->belongsTo('App\Model\Admin\Mapel');
    }

    public function jawaban()
    {
      return $this->hasMany('App\Model\Guru\Jawaban');
    }

    public function nilai()
    {
      return $this->hasMany('App\Model\Guru\Nilai');
    }

    public function diskusi()
    {
      return $this->hasMany('App\Model\Guru\Diskusi');
    }

    public function soal()
    {
      return $this->hasMany('App\Model\Guru\Soal');
    }
}
