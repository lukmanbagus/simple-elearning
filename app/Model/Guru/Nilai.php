<?php

namespace App\Model\Guru;

use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    //
    protected $table = 'nilai';
    protected $fillable = [
      'materi_id','siswa_id','nilai'
    ];

    public function siswa()
    {
      return $this->belongsTo('App\Model\Admin\Siswa');
    }

    public function materi()
    {
      return $this->belongsTo('App\Model\Guru\Materi');
    }
}
