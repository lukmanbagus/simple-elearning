<?php

namespace App\Model\Guru;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    //
    protected $table = 'soal';
    protected $fillable = ['materi_id','soal','a','b','c','d','jawaban'];

    public function materi()
    {
      return $this->belongsTo('App\Model\Guru\Materi');
    }
}
