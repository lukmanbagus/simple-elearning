<?php

namespace App\Model\Siswa;

use Illuminate\Database\Eloquent\Model;

class HasilJawabanSiswa extends Model
{
    //
    protected $table = 'nilai_hasil_jawaban';
    protected $fillable = ['materi_id','siswa_id','nilai'];

    public function siswa()
    {
      return $this->belongsTo('App\Model\Admin\Siswa');
    }
}
