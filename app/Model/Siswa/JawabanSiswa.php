<?php

namespace App\Model\Siswa;

use Illuminate\Database\Eloquent\Model;

class JawabanSiswa extends Model
{
    //
    protected $table = 'jawaban_siswa';
    protected $fillable = ['soal_id','siswa_id','jawaban'];
}
