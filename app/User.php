<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function jawabans()
    {
      return $this->hasMany('App\Model\Guru\Jawaban');
    }

    public function siswa()
    {
      return $this->hasOne('App\Model\Admin\Siswa');
    }

    public function guru()
    {
      return $this->hasOne('App\Model\Admin\Guru');
    }

    public function diskusi()
    {
      return $this->hasMany('App\Model\Guru\DiskusiContent');
    }
}
