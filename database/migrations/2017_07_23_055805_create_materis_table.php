<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_materi');
            $table->string('content');
            $table->string('file')->nullable();
            $table->integer('mapel_id')->unsigned();
            $table->foreign('mapel_id')->references('id')->on('mapel')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('materi_kelas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('materi_id')->unsigned();
            $table->integer('kelas_id')->unsigned();
            $table->timestamps();


            $table->foreign('materi_id')->references('id')->on('materi');
            $table->foreign('kelas_id')->references('id')->on('kelas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materi_kelas');
        Schema::dropIfExists('materi');
    }
}
