<?php

use Illuminate\Database\Seeder;
use App\Model\Admin\Guru;

class GuruSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Guru::create([
        'nip' => '1234567890',
        'nama' => 'Icha Cantik',
        'tempat_lahir' => 'Tegal',
        'tgl_lahir' => '1994-11-12',
        'alamat' => 'Adiwerna',
        'jenis_kelamin' => 'Perempuan',
        'no_telp' => '085612341234',
        'email' => 'icha@gmail.com',
        'user_id' => 2,
        'mapel_id' => 1
      ]);
    }
}
