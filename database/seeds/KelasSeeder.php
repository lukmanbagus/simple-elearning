<?php

use Illuminate\Database\Seeder;
use App\Model\Admin\Kelas;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kelas::create([
          'kelas' => '2 IPA 1',
          'jurusan' => 'IPA'
        ]);

        Kelas::create([
          'kelas' => '2 IPA 2',
          'jurusan' => 'IPA'
        ]);
    }
}
