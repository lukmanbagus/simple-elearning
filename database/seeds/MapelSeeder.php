<?php

use Illuminate\Database\Seeder;
use App\Model\Admin\Mapel;

class MapelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Mapel::create([
          'nama_mapel' => 'Matematika',
          'keterangan' => 'Matematika Dasar'
        ]);

        Mapel::create([
          'nama_mapel' => 'Fisika',
          'keterangan' => 'Fisika Dasar'
        ]);
    }
}
