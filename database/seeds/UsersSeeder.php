<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::create([
        'name' => 'Admin',
        'email' => 'admin@gmail.com',
        'password' => bcrypt('admin'),
        'role' => 'admin'
      ]);

      User::create([
        'name' => 'Icha',
        'email' => 'icha@gmail.com',
        'password' => bcrypt('guru'),
        'role' => 'guru'
      ]);
    }
}
