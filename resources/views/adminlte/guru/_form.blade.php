<div class="form-group {{ $errors->has('nip') ? 'has-error' : '' }}">
  <label class="control-label col-sm-2">NIP</label>
  <div class="col-sm-8">
    {!! Form::text('nip',null,['class' => 'form-control']) !!}
    {!! $errors->first('nip','<span>:message</span>') !!}
  </div>
</div>
<div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
  <label class="control-label col-sm-2">Nama</label>
  <div class="col-sm-8">
    {!! Form::text('nama',null,['class' => 'form-control']) !!}
    {!! $errors->first('nama','<span>:message</span>') !!}
  </div>
</div>
<div class="form-group {{ $errors->has('tempat_lahir') ? 'has-error' : '' }}">
  <label class="control-label col-sm-2">Tempat Lahir</label>
  <div class="col-sm-8">
    {!! Form::text('tempat_lahir',null,['class' => 'form-control']) !!}
    {!! $errors->first('tempat_lahir','<span>:message</span>') !!}
  </div>
</div>
<div class="form-group {{ $errors->has('tgl_lahir') ? 'has-error' : '' }}">
  <label class="control-label col-sm-2">Tanggal Lahir</label>
  <div class="col-sm-8">
    {!! Form::text('tgl_lahir',null,['class' => 'form-control datepicker']) !!}
    {!! $errors->first('tgl_lahir','<span>:message</span>') !!}
  </div>
</div>
<div class="form-group {{ $errors->has('alamat') ? 'has-error' : '' }}">
  <label class="control-label col-sm-2">Alamat</label>
  <div class="col-sm-8">
    {!! Form::text('alamat',null,['class' => 'form-control']) !!}
    {!! $errors->first('alamat','<span>:message</span>') !!}
  </div>
</div>
<div class="form-group {{ $errors->has('jenis_kelamin') ? 'has-error' : '' }}">
  <label class="control-label col-sm-2">Jenis Kelamin</label>
  <div class="col-sm-8">
    {!!
      Form::select(
        'jenis_kelamin',
        [
          'laki-laki' => 'Laki-laki',
          'perempuan' => 'Perempuan',
        ],
        null,
        [
          'class' => 'form-control'
        ]
      )
    !!}
    {!! $errors->first('jenis_kelamin','<span>:message</span>') !!}
  </div>
</div>
<div class="form-group {{ $errors->has('no_telp') ? 'has-error' : '' }}">
  <label class="control-label col-sm-2">No Telp</label>
  <div class="col-sm-8">
    {!! Form::text('no_telp',null,['class' => 'form-control']) !!}
    {!! $errors->first('no_telp','<span>:message</span>') !!}
  </div>
</div>
<div class="form-group {{ $errors->has('mapel_id') ? 'has-error' : '' }}">
  <label class="control-label col-sm-2">Mapel</label>
  <div class="col-sm-8">
    {!!
      Form::select(
        'mapel_id',
        App\Model\Admin\Mapel::pluck('nama_mapel','id')->all(),
        null,
        [
          'class' => 'form-control'
        ]
      )
    !!}
    {!! $errors->first('mapel_id','<span>:message</span>') !!}
  </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
  <label class="control-label col-sm-2">Email</label>
  <div class="col-sm-8">
    {!! Form::email('email',null,['class' => 'form-control']) !!}
    {!! $errors->first('email','<span>:message</span>') !!}
  </div>
</div>
<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
  <label class="control-label col-sm-2">Password</label>
  <div class="col-sm-8">
    {!! Form::password('password',['class' => 'form-control','placeholder' => 'Password for login']) !!}
    {!! $errors->first('password','<span>:message</span>') !!}
  </div>
</div>
