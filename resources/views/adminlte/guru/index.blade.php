@extends('adminlte.layouts.master')
@section('page_header')
  <i class="fa fa-address-card"></i> Guru
@endsection
@section('breadcrumb')
  <li><a href="{{ url('/home') }}">Home</a></li>
  <li class="active">Guru</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-th-list fa-fw"></i> Daftar Guru</h3>
  </div>
  <table class="table table-striped table-bordered table-responsive table-hover table-condensed">
    <thead>
      <tr>
        <th>#</th>
        <th>NIP</th>
        <th>Nama</th>
        <th>TTL</th>
        <th>Alamat</th>
        <th>Jenis Kelamin</th>
        <th>No Telp</th>
        <th>Email</th>
        <th>Mapel</th>
        <th>Tools</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1;?>
      @foreach($data as $guru)
      <tr>
        <td>{{ $no++ }}</td>
        <td>{{ $guru->nip }}</td>
        <td>{{ $guru->nama }}</td>
        <td>{{ $guru->tempat_lahir . ', ' . $guru->tgl_lahir }}</td>
        <td>{{ $guru->alamat }}</td>
        <td>{{ $guru->jenis_kelamin }}</td>
        <td>{{ $guru->no_telp }}</td>
        <td>{{ $guru->email }}</td>
        <td>{{ $guru->mapel->nama_mapel }}</td>
        <td>
          {!! Form::open(['url' => action('Admin\GuruController@destroy',$guru->id),'method' => 'DELETE']) !!}
          <a href="{{ action('Admin\GuruController@edit',$guru->id) }}" class="btn btn-sm btn-warning">Edit</a>
          <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
          {!! Form::close() !!}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div class="box-footer">
    {{ $data->links() }}
  </div>
</div>
@endsection
