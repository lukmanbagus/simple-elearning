<div class="form-group {{ $errors->has('kelas') ? 'has-error' : '' }}">
  <label class="control-label col-sm-2">Nama Kelas</label>
  <div class="col-sm-8">
    {!! Form::text('kelas',null,['class' => 'form-control']) !!}
    {!! $errors->first('kelas','<span>:message</span>') !!}
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-2">Jurusan</label>
  <div class="col-sm-8">
    {!! Form::text('jurusan',null,['class' => 'form-control']) !!}
  </div>
</div>
