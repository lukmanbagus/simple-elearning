@extends('adminlte.layouts.master')
@section('page_header')
  <i class="fa fa-flag"></i> Kelas
@endsection
@section('breadcrumb')
  <li><a href="{{ url('/home') }}">Home</a></li>
  <li><a href="{{ url('/kelas') }}">Kelas</a></li>
  <li class="active">Create</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-user fa-fw"></i> Buat Kelas Baru</h3>
  </div>
  <div class="box-body">
    {!! Form::open(['url' => action('Admin\KelasController@store'),'class' => 'form-horizontal']) !!}
      @include('adminlte.kelas._form')
      <div class="form-group">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-4">
          {!! Form::submit('Buat Kelas',['class' => 'btn btn-success']) !!}
          <a href="{{ action('Admin\KelasController@index') }}" class="btn btn-default">Batal</a>
        </div>
      </div>
    {!! Form::close() !!}
  </div>
</div>
@endsection
