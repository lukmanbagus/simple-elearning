@extends('adminlte.layouts.master')
@section('page_header')
  <i class="fa fa-flag"></i> Kelas
@endsection
@section('breadcrumb')
  <li><a href="{{ url('/home') }}">Home</a></li>
  <li class="active">Kelas</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-th-list fa-fw"></i> Daftar Kelas</h3>
  </div>
  <table class="table table-striped table-bordered table-responsive table-hover table-condensed">
    <thead>
      <tr>
        <th>#</th>
        <th>Kelas</th>
        <th>Jurusan</th>
        <th>Tools</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1;?>
      @foreach($data as $kelas)
      <tr>
        <td>{{ $no++ }}</td>
        <td>{{ $kelas->kelas }}</td>
        <td>{{ $kelas->jurusan }}</td>
        <td>
          {!! Form::open(['url' => action('Admin\KelasController@destroy',$kelas->id),'method' => 'DELETE']) !!}
          <a href="{{ action('Admin\KelasController@edit',$kelas->id) }}" class="btn btn-sm btn-warning">Edit</a>
          <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
          {!! Form::close() !!}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div class="box-footer">
    {{ $data->links() }}
  </div>
</div>
@endsection
