<div class="form-group {{ $errors->has('nama_mapel') ? 'has-error' : '' }}">
  <label class="control-label col-sm-2">Nama Mapel</label>
  <div class="col-sm-8">
    {!! Form::text('nama_mapel',null,['class' => 'form-control']) !!}
    {!! $errors->first('nama_mapel','<span>:message</span>') !!}
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-2">Keterangan</label>
  <div class="col-sm-8">
    {!! Form::text('keterangan',null,['class' => 'form-control']) !!}
  </div>
</div>
