@extends('adminlte.layouts.master')
@section('page_header')
  <i class="fa fa-file"></i> Mapel
@endsection
@section('breadcrumb')
  <li><a href="{{ url('/home') }}">Home</a></li>
  <li><a href="{{ url('/mapel') }}">Mapel</a></li>
  <li class="active">Create</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-file fa-fw"></i> Buat Mapel Baru</h3>
  </div>
  <div class="box-body">
    {!! Form::open(['url' => action('Admin\MapelController@store'),'class' => 'form-horizontal']) !!}
      @include('adminlte.mapel._form')
      <div class="form-group">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-4">
          {!! Form::submit('Buat Mapel',['class' => 'btn btn-success']) !!}
          <a href="{{ action('Admin\MapelController@index') }}" class="btn btn-default">Batal</a>
        </div>
      </div>
    {!! Form::close() !!}
  </div>
</div>
@endsection
