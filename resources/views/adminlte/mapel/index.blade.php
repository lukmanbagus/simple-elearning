@extends('adminlte.layouts.master')
@section('page_header')
  <i class="fa fa-file"></i> Mapel
@endsection
@section('breadcrumb')
  <li><a href="{{ url('/home') }}">Home</a></li>
  <li class="active">Mapel</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-th-list fa-fw"></i> Daftar Mapel</h3>
  </div>
  <table class="table table-striped table-bordered table-responsive table-hover table-condensed">
    <thead>
      <tr>
        <th>#</th>
        <th>Mapel</th>
        <th>Keterangan</th>
        <th>Tools</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1;?>
      @foreach($data as $mapel)
      <tr>
        <td>{{ $no++ }}</td>
        <td>{{ $mapel->nama_mapel }}</td>
        <td>{{ $mapel->keterangan }}</td>
        <td>
          {!! Form::open(['url' => action('Admin\MapelController@destroy',$mapel->id),'method' => 'DELETE']) !!}
          <a href="{{ action('Admin\MapelController@edit',$mapel->id) }}" class="btn btn-sm btn-warning">Edit</a>
          <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
          {!! Form::close() !!}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div class="box-footer">
    {{ $data->links() }}
  </div>
</div>
@endsection
