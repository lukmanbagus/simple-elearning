@extends('adminlte.layouts.master')
@section('page_header')
  <i class="fa fa-address-card"></i> Siswa
@endsection
@section('breadcrumb')
  <li><a href="{{ url('/home') }}">Home</a></li>
  <li><a href="{{ url('/siswa') }}">Siswa</a></li>
  <li class="active">Edit</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-edit fa-fw"></i> Edit Siswa {{ $siswa->nama }}</h3>
  </div>
  <div class="box-body">
    {!! Form::model($siswa,['url' => action('Admin\SiswaController@update',$siswa->id),'class' => 'form-horizontal','method' => 'PUT']) !!}
      @include('adminlte.siswa._form')
      <div class="form-group">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-4">
          {!! Form::submit('Update Siswa',['class' => 'btn btn-success']) !!}
          <a href="{{ action('Admin\SiswaController@index') }}" class="btn btn-default">Batal</a>
        </div>
      </div>
    {!! Form::close() !!}
  </div>
</div>
@endsection
