@extends('adminlte.layouts.master')
@section('page_header')
  <i class="fa fa-address-card"></i> Siswa
@endsection
@section('breadcrumb')
  <li><a href="{{ url('/home') }}">Home</a></li>
  <li class="active">Siswa</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-th-list fa-fw"></i> Daftar Siswa</h3>
  </div>
  <table class="table table-striped table-bordered table-responsive table-hover table-condensed">
    <thead>
      <tr>
        <th>#</th>
        <th>NIS</th>
        <th>Nama</th>
        <th>TTL</th>
        <th>Alamat</th>
        <th>Jenis Kelamin</th>
        <th>No Telp</th>
        <th>Email</th>
        <th>Angkatan</th>
        <th>Kelas</th>
        <th>Tools</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1;?>
      @foreach($data as $siswa)
      <tr>
        <td>{{ $no++ }}</td>
        <td>{{ $siswa->nis }}</td>
        <td>{{ $siswa->nama }}</td>
        <td>{{ $siswa->tempat_lahir . ', ' . $siswa->tgl_lahir }}</td>
        <td>{{ $siswa->alamat }}</td>
        <td>{{ $siswa->jenis_kelamin }}</td>
        <td>{{ $siswa->no_telp }}</td>
        <td>{{ $siswa->email }}</td>
        <td>{{ $siswa->angkatan }}</td>
        <td>{{ $siswa->kelas->kelas }}</td>
        <td>
          {!! Form::open(['url' => action('Admin\SiswaController@destroy',$siswa->id),'method' => 'DELETE']) !!}
          <a href="{{ action('Admin\SiswaController@edit',$siswa->id) }}" class="btn btn-sm btn-warning">Edit</a>
          <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
          {!! Form::close() !!}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div class="box-footer">
    {{ $data->links() }}
  </div>
</div>
@endsection
