<div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
  <label class="control-label col-sm-2">Nama</label>
  <div class="col-sm-8">
    {!! Form::text('name',null,['class' => 'form-control']) !!}
  </div>
</div>
<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
  <label class="control-label col-sm-2">Email</label>
  <div class="col-sm-8">
    {!! Form::email('email',null,['class' => 'form-control']) !!}
  </div>
</div>
<div class="form-group {!! $errors->has('role') ? 'has-error' : '' !!}">
  <label class="control-label col-sm-2">Level</label>
  <div class="col-sm-8">
    {!!
      Form::select(
        'role',
        [
          'admin' => 'Admin',
          'guru' => 'Guru',
          'siswa' => 'Siswa',
        ],
        null,
        [
          'class' => 'form-control'
        ]
      )
    !!}
  </div>
</div>
<div class="form-group {!! $errors->has('password') ? 'has-error' : '' !!}">
  <label class="control-label col-sm-2">Password</label>
  <div class="col-sm-8">
    {!! Form::password('password',['class' => 'form-control']) !!}
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-2">Ulangi Password</label>
  <div class="col-sm-8">
    {!! Form::password('password_confirmation',['class' => 'form-control']) !!}
  </div>
</div>
