@extends('adminlte.layouts.master')
@section('page_header')
  <i class="fa fa-group"></i> User
@endsection
@section('breadcrumb')
  <li><a href="{{ url('/home') }}">Home</a></li>
  <li><a href="{{ url('/user') }}">User</a></li>
  <li class="active">Create</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-user fa-fw"></i> Edit User {{ $user->name }}</h3>
  </div>
  <div class="box-body">
    {!! Form::model($user,['route' => ['user.update',$user->id],'class' => 'form-horizontal','method' => 'PUT']) !!}
      @include('adminlte.user._form')
      <div class="form-group">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-4">
          {!! Form::submit('Update User',['class' => 'btn btn-success']) !!}
          <a href="{{ action('Admin\UserController@index') }}" class="btn btn-default">Batal</a>
        </div>
      </div>
    {!! Form::close() !!}
  </div>
</div>
@endsection
