@extends('adminlte.layouts.master')
@section('page_header')
  <i class="fa fa-group"></i> User
@endsection
@section('breadcrumb')
  <li><a href="{{ url('/home') }}">Home</a></li>
  <li class="active">User</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-th-list fa-fw"></i> Daftar User</h3>
  </div>
  <table class="table table-striped table-bordered table-responsive table-hover table-condensed">
    <thead>
      <tr>
        <th>#</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Level</th>
        <th>Tools</th>
      </tr>
    </thead>
    <tbody>
      <?php $no = 1; ?>
      @foreach($users as $user)
      <tr>
        <td>{{ $no++ }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->role }}</td>
        <td>
          {!! Form::open(['url' => action('Admin\UserController@destroy',$user->id),'method' => 'DELETE']) !!}
          <a href="{{ action('Admin\UserController@edit',$user->id) }}" class="btn btn-sm btn-warning">Edit</a>
          <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
          {!! Form::close() !!}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div class="box-footer">
    {{ $users->links() }}
  </div>
</div>
@endsection
