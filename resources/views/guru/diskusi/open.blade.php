@extends('adminlte.layouts.master')
@section('page_header')
  Ruang Diskusi
@endsection
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li class="active">Materi</li>
@endsection
@section('content')
<div class="box box-success">
  <div class="box-header">
    <i class="fa fa-comments-o"></i>
    <h3 class="box-title">Diskusi Materi {{ $data->materi->nama_materi }}</h3>
  </div>
  <div class="box-body chat" id="chat-box">
    @foreach($data->diskusi_content as $diskusi)
    <!-- chat item -->
    <div class="item">
      <img src="{{ url('adminlte/dist/img/avatar.png') }}" alt="user image" class="online">

      <p class="message">
        <a href="#" class="name">
          <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{ $diskusi->created_at }}</small>
          @if($diskusi->user->id == \Auth::user()->id)
            Anda
          @else
            {{ $diskusi->user->name }}
          @endif
        </a>
        {{ $diskusi->content }}
      </p>
    </div>
    <!-- /.item -->
    @endforeach
  </div>
  <!-- /.chat -->
  <div class="box-footer">
    {!! Form::open(['url' => action('Guru\DiskusiController@store')]) !!}
    <div class="input-group">
      {!! Form::hidden('diskusi_id',$data->id) !!}
      {!! Form::hidden('user_id',\Auth::user()->id) !!}
      {!! Form::text('content',null,['class' => 'form-control','placeholder' => 'Tulis pesan disini']) !!}
      <div class="input-group-btn">
        <button type="submit" class="btn btn-success">Kirim pesan</button>
      </div>
    </div>
    {!! Form::close() !!}
  </div>
</div>
@endsection
