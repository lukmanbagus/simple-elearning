@extends('adminlte.layouts.master')
@section('page_header','Jawaban')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li class="active">Materi</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <table class="table table-striped table-bordered table-responsive table-hover table-condensed">
      <thead>
        <tr>
          <th>#</th>
          <th>Untuk Materi</th>
          <th>Nama Siswa</th>
          <th>Kelas</th>
          <th>Tanggal</th>
          <th>Tools</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;?>
        @foreach($data as $materi)
          @foreach($materi->jawaban as $jawaban)
          <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $jawaban->materi->nama_materi }}</td>
            <td>{{ $jawaban->user->name }}</td>
            <td>{{ $jawaban->user->siswa->kelas->kelas }}</td>
            <td>{{ $jawaban->created_at }}</td>
            <td>
              <a href="{{ url('/'.'files/'.$jawaban->file) }}" target="_blank" class="btn btn-sm btn-warning">Download File</a>
            </td>
          </tr>
          @endforeach
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
