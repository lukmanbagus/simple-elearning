<div class="form-group">
  <label class="control-label col-sm-2">Nama Materi</label>
  <div class="col-sm-8">
    {!! Form::text('nama_materi',null,['class' => 'form-control']) !!}
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-2">Materi</label>
  <div class="col-sm-8">
    {!! Form::textarea('content',null,['class' => 'form-control','id' => 'summernote']) !!}
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-2">File (.zip)</label>
  <div class="col-sm-8">
    {!! Form::file('file',['class' => 'form-control']) !!}
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-2">Akses Kelas</label>
  <div class="col-sm-8">
    @foreach(App\Model\Admin\Kelas::get() as $kelas)
    <div class="checkbox">
      <label>
        {!! Form::checkbox('kelas[]',$kelas->id) !!} {{ $kelas->kelas }}
      </label>
    </div>
    @endforeach
  </div>
</div>
