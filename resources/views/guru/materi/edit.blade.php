@extends('adminlte.layouts.master')
@section('page_header','Materi')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li><a href="#">Materi</a></li>
  <li class="active">Update</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Update Materi</h3>
  </div>
  <div class="box-body">
    {!! Form::model($materi,['url' => action('Guru\MateriController@update',$materi->id),'class' => 'form-horizontal','files' => true,'method' => 'PUT']) !!}
      @include('guru.materi._form')
      <div class="form-group">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-4">
          {!! Form::submit('Update Materi',['class' => 'btn btn-success']) !!}
          <a href="{{ action('Guru\MateriController@index') }}" class="btn btn-default">Batal</a>
        </div>
      </div>
    {!! Form::close() !!}
  </div>
</div>
@endsection
