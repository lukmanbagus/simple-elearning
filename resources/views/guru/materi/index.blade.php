@extends('adminlte.layouts.master')
@section('page_header','Materi')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li class="active">Materi</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <table class="table table-striped table-bordered table-responsive table-hover table-condensed">
      <thead>
        <tr>
          <th>#</th>
          <th>Materi</th>
          <th>Tanggal</th>
          <th>Kelas</th>
          <th>Tools</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;?>
        @foreach($data as $materi)
        <tr>
          <td>{{ $no++ }}</td>
          <td>{{ $materi->nama_materi }}</td>
          <td>{{ $materi->created_at }}</td>
          <td>
            <ul>
              @foreach($materi->kelas as $kelas)
                <li>{{ $kelas->kelas }}</li>
              @endforeach
            </ul>
          </td>
          <td>
            {!! Form::open(['url' => action('Guru\MateriController@destroy',$materi->id),'method' => 'DELETE']) !!}
            <a href="{{ action('Guru\MateriController@edit',$materi->id) }}" class="btn btn-sm btn-warning">Edit</a>
            <a href="{{ action('Guru\MateriController@listsiswa',$materi->id) }}" class="btn btn-sm btn-success">Masukan Nilai</a>
            <a href="{{ action('Guru\MateriController@soalLatihan',$materi->id) }}" class="btn btn-sm btn-default">Soal Latihan</a>
            @if(App\Model\Guru\Diskusi::where('materi_id',$materi->id)->count() == 0)
            <a href="{{ action('Guru\DiskusiController@create',$materi->id) }}" class="btn btn-sm btn-info">Buat Diskusi</a>
            @endif
            <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
            {!! Form::close() !!}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div class="box-footer">
    </div>
  </div>
</div>
@endsection
