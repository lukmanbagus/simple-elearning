@extends('adminlte.layouts.master')
@section('page_header','Daftar Siswa Yang Ikut Serta')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li class="active">Daftar Siswa</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    @foreach($data as $kelas)
    <table class="table table-striped table-bordered table-responsive table-hover table-condensed">
      <thead>
        <tr>
          <th colspan="10">Kelas : {{ $kelas->kelas }}</th>
        </tr>
        <tr>
          <th>#</th>
          <th>NIS</th>
          <th>Nama Siswa</th>
          <th>Nilai Tugas</th>
          <th>Tools</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;?>
        @foreach($kelas->siswa as $siswa)
        <tr>
          <td>{{ $no++ }}</td>
          <td>{{ $siswa->nis }}</td>
          <td>{{ $siswa->nama }}</td>
          <td>{{ ($siswa->nilai->where('materi_id',$materi_id)->first() != null) ? $siswa->nilai->where('materi_id',$materi_id)->first()['nilai'] : 0 }}</td>
          <td>
            @if(App\Model\Guru\Nilai::where('siswa_id',$siswa->id)->where('materi_id',$materi_id)->count() > 0)
            <a href="{{ action('Guru\MateriController@editnilai',[$materi_id,$siswa->id]) }}" class="btn btn-sm btn-warning">Edit Nilai</a>
            @else
            <a href="{{ action('Guru\MateriController@inputnilai',[$materi_id,$siswa->id]) }}" class="btn btn-sm btn-success">Tambah Nilai</a>
            @endif
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    @endforeach
    <div class="box-footer">
    </div>
  </div>
</div>
@endsection
