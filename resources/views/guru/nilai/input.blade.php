@extends('adminlte.layouts.master')
@section('page_header','Input Nilai Siswa')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li class="active">Input Nilai Siswa</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    Input Nilai {{ $siswa->nama }}
  </div>
  <div class="box-body">
    {!! Form::open(['route' => 'materi.nilai.store']) !!}
        {!! Form::hidden('siswa_id',$siswa->id) !!}
        {!! Form::hidden('materi_id',$materi_id) !!}
        <div class="form-group">
          <label>Nilai Tugas</label>
          <div class="col-sm-5 input-group">
            {!! Form::text('nilai',null,['class' => 'form-control']) !!}
            <span class="input-group-btn">
              <button type="submit" class="btn btn-success">Input</button>
            </span>
          </div>
        </div>
    {!! Form::close() !!}
  </div>
  <div class="box-footer">
    <a href="{{ route('materi.listsiswa',$materi_id) }}" class="btn btn-default">Kembali</a>
  </div>
</div>
@endsection
