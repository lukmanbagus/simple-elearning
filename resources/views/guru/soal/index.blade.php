@extends('adminlte.layouts.master')
@section('page_header','Soal')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li class="active">Soal</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <a href="{{ url('/materi') }}" class="btn btn-default brn-sm">Kembali</a>
    <a href="{{ route('materi.create-soal-latihan',$id_materi) }}" class="btn btn-primary brn-sm">Tambah Soal</a>
  </div>
  <div class="box-body">
    <table class="table table-striped table-bordered table-responsive table-hover table-condensed">
      <thead>
        <tr>
          <th>#</th>
          <th>Soal</th>
          <th>Pilihan Jawaban</th>
          <th>Jawaban</th>
          <th>Tools</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;?>
        @foreach($soals as $soal)
        <tr>
          <td>{{ $no++ }}</td>
          <td>{!! $soal->soal !!}</td>
          <td>
            <ol type="a">
              <li>{{ $soal->a }}</li>
              <li>{{ $soal->b }}</li>
              <li>{{ $soal->c }}</li>
              <li>{{ $soal->d }}</li>
            </ol>
          </td>
          <td>
            {{ $soal->jawaban }}
          </td>
          <td>
            {!! Form::open(['url' => action('Guru\MateriController@soalDestroy',$soal->id),'method' => 'DELETE']) !!}
            <a href="{{ action('Guru\MateriController@editSoal',$soal->id) }}" class="btn btn-sm btn-warning">Edit</a>
            <button type="submit" onclick="confirm('Anda yakin ingin menghapus data ini ?')" class="btn btn-sm btn-danger">Hapus</button>
            {!! Form::close() !!}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
