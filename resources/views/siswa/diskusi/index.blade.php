@extends('adminlte.layouts.master')
@section('page_header','Diskusi')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li class="active">Materi</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    Daftar Diskusi Per Materi
  </div>
  <div class="box-body">
    <table class="table table-striped table-bordered table-responsive table-hover table-condensed">
      <thead>
        <tr>
          <th>#</th>
          <th>Materi</th>
          <th>Tanggal</th>
          <th>Tools</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;?>
        @foreach($data as $materi)
          @foreach($materi->diskusi as $diskusi)
          <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $diskusi->materi->nama_materi }}</td>
            <td>{{ $diskusi->created_at }}</td>
            <td>
              <a href="{{ action('Siswa\DiskusiController@open',$diskusi->id) }}" class="btn btn-primary btn-sm">Buka Diskusi</a>
            </td>
          </tr>
          @endforeach
        @endforeach
      </tbody>
    </table>
    <div class="box-footer">
      {{-- $data->links() --}}
    </div>
  </div>
</div>
@endsection
