@extends('adminlte.layouts.master')
@section('page_header','File Tugas Terkirim')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li class="active">Materi</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <table class="table table-striped table-bordered table-responsive table-hover table-condensed">
      <thead>
        <tr>
          <th>#</th>
          <th>Nama File</th>
          <th>Untuk Mapel</th>
          <th>Tanggal Terkirim</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;?>
        @foreach($tugas as $file)
        <tr>
          <td>{{ $no++ }}</td>
          <td><a href="{{ url('/' . 'files/' . $file->file) }}" target="_blank">{{ $file->file }}</a></td>
          <td>{{ $file->materi->mapel->nama_mapel }}</td>
          <td>{{ $file->created_at }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
