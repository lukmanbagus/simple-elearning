@extends('adminlte.layouts.master')
@section('page_header','Materi')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li class="active">Materi</li>
@endsection
@section('content')
@if(Session::has('success'))
<div class="alert alert-success">
  <p>{{ Session::get('success') }}</p>
</div>
@endif
<div class="box box-primary">
  <div class="box-header with-border">
    <table class="table table-striped table-bordered table-responsive table-hover table-condensed">
      <thead>
        <tr>
          <th>#</th>
          <th>Materi</th>
          <th>Mapel</th>
          <th>Guru</th>
          <th>Tanggal</th>
          <th>Nilai Soal Latihan</th>
          <th>Tools</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;?>
        @foreach($kelas->materi as $materi)
        <?php $hasilnilai = App\Model\Siswa\HasilJawabanSiswa::where('materi_id',$materi->id)->where('siswa_id',\Auth::user()->siswa->id)->first();?>
        <tr>
          <td>{{ $no++ }}</td>
          <td>{{ $materi->nama_materi }}</td>
          <td>{{ $materi->mapel->nama_mapel }}</td>
          <td>{{ $materi->mapel->guru->nama }}</td>
          <td>{{ $materi->created_at }}</td>
          <td>{{ (count($hasilnilai)) ? $hasilnilai->nilai : ''  }}</td>
          <td>
            <a href="{{ action('Siswa\MateriController@masuk',$materi->id) }}" class="btn btn-sm btn-primary">Masuk</a>
            @if(count($materi->soal) > 0 && App\Model\Siswa\HasilJawabanSiswa::where('materi_id',$materi->id)->where('siswa_id',\Auth::user()->siswa->id)->get()->count() == 0)
              <a href="{{ action('Siswa\MateriController@soalLatihan',$materi->id) }}" class="btn btn-sm btn-default">Soal Latihan</a>
            @endif
            <a href="{{ action('Siswa\MateriController@tugas',$materi->id) }}" class="btn btn-sm btn-success">Upload Tugas</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <div class="box-footer">
      {{-- $data->links() --}}
    </div>
  </div>
</div>
@endsection
