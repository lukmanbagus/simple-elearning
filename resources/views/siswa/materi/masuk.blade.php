@extends('adminlte.layouts.master')
@section('page_header','Materi')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li class="active">Materi</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">{{ $materi->nama_materi }}</h3>
    <span class="text-muted pull-right">Disusun Oleh : {{$materi->mapel->guru->nama}}</span>
  </div>
  <div class="box-body">
    <p>
      {!! $materi->content !!}
    </p>
    <hr>
    @if($materi->file != null)
    <a href="{{ url('/files/'.$materi->file) }}" class="btn btn-primary btn-sm" target="_blank"><i class="fa fa-file"></i> Download File</a>
    @else
    Tidak ada file terlampir
    @endif
  </div>
</div>
@endsection
