@extends('adminlte.layouts.master')
@section('page_header','Materi')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li class="active">Materi</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Upload Tugas untuk materi {{ $materi->nama_materi }}</h3>
  </div>
  <div class="box-body">
    {!! Form::open(['url' => action('Siswa\MateriController@uploadtugas'),'files' => true]) !!}
        <label class="control-label col-sm-2">File Tugas (.zip)</label>
        <div class="col-sm-6">
          {!! Form::hidden('materi_id',$materi->id) !!}
          {!! Form::file('file') !!}
        </div>
        <button type="submit" class="btn btn-primary btn-sm">Upload File Tugas</button>
    {!! Form::close() !!}
  </div>
</div>
@endsection
