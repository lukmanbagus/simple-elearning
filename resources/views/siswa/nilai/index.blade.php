@extends('adminlte.layouts.master')
@section('page_header','Nilai Tugas')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li class="active">Nilai</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <table class="table table-striped table-bordered table-responsive table-hover table-condensed">
      <thead>
        <tr>
          <th>#</th>
          <th>Nama Mapel</th>
          <th>Nama Materi</th>
          <th>Nilai</th>
          <th>Tanggal Publishth</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;?>
        @foreach($data as $nilai)
        <tr>
          <td>{{ $no++ }}</td>
          <td>{{ $nilai->materi->mapel->nama_mapel }}</td>
          <td>{{ $nilai->materi->nama_materi }}</td>
          <td>{{ $nilai->nilai }}</td>
          <td>{{ $nilai->created_at }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
