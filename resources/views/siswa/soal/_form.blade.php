{!! Form::hidden('materi_id',$materi->id) !!}
<div class="form-group">
  <label class="control-label col-sm-2">Soal</label>
  <div class="col-sm-8">
    {!! Form::textarea('soal',null,['class' => 'form-control','id' => 'summernote']) !!}
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-2">A.</label>
  <div class="col-sm-8">
    {!! Form::text('a',null,['class' => 'form-control']) !!}
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-2">B.</label>
  <div class="col-sm-8">
    {!! Form::text('b',null,['class' => 'form-control']) !!}
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-2">C.</label>
  <div class="col-sm-8">
    {!! Form::text('c',null,['class' => 'form-control']) !!}
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-2">D.</label>
  <div class="col-sm-8">
    {!! Form::text('d',null,['class' => 'form-control']) !!}
  </div>
</div>
<div class="form-group">
  <label class="control-label col-sm-2">Jawaban</label>
  <div class="col-sm-8">
    {!! Form::select('jawaban',
    [
      'a' => 'A',
      'b' => 'B',
      'c' => 'C',
      'd' => 'D'
    ],
    null,
    ['class' => 'form-control']) !!}
  </div>
</div>
