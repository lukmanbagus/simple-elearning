@extends('adminlte.layouts.master')
@section('page_header','Soal')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li><a href="#">Soal</a></li>
  <li class="active">Create</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Buat Soal Baru</h3>
  </div>
  <div class="box-body">
    {!! Form::open(['url' => action('Guru\MateriController@storeSoal'),'class' => 'form-horizontal','files' => true]) !!}
      @include('guru.soal._form')
      <div class="form-group">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-4">
          {!! Form::submit('Buat Soal',['class' => 'btn btn-success']) !!}
          <a href="{{ action('Guru\MateriController@soalLatihan',$materi->id) }}" class="btn btn-default">Batal</a>
        </div>
      </div>
    {!! Form::close() !!}
  </div>
</div>
@endsection
