@extends('adminlte.layouts.master')
@section('page_header','Soal')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li><a href="#">Soal</a></li>
  <li class="active">Update</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Update Soal</h3>
  </div>
  <div class="box-body">
    {!! Form::model($soal,['url' => action('Guru\MateriController@updateSoal',$soal->id),'class' => 'form-horizontal','files' => true,'method' => 'PUT']) !!}
      @include('guru.soal._form')
      <div class="form-group">
        <label class="control-label col-sm-2"></label>
        <div class="col-sm-4">
          {!! Form::submit('Update Soal',['class' => 'btn btn-success']) !!}
          <a href="{{ action('Guru\MateriController@soalLatihan',$materi->id) }}" class="btn btn-default">Batal</a>
        </div>
      </div>
    {!! Form::close() !!}
  </div>
</div>
@endsection
