@extends('adminlte.layouts.master')
@section('page_header','Soal')
@section('breadcrumb')
  <li><a href="{{ url('home') }}">Home</a></li>
  <li class="active">Soal</li>
@endsection
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <a href="{{ url('/materiku') }}" class="btn btn-default brn-sm">Kembali</a>
  </div>
  <div class="box-body">
    {!! Form::open(['url' => action('Siswa\MateriController@jawabSoalLatihan',$id_materi)]) !!}
    <ol type="1">
      @foreach($soals as $soal)
        <li>
          <p>
            {!! $soal->soal !!}
            <ol type="a">
              <li><input type="radio" name="{{ $soal->id }}" value="a"> {{ $soal->a }}</li>
              <li><input type="radio" name="{{ $soal->id }}" value="b"> {{ $soal->b }}</li>
              <li><input type="radio" name="{{ $soal->id }}" value="c"> {{ $soal->c }}</li>
              <li><input type="radio" name="{{ $soal->id }}" value="d"> {{ $soal->d }}</li>
            </ol>
          </p>
        </li>
      @endforeach
    </ol>
    <div class="alert alert-warning">Jawaban yang telah dikirim tidak dapat diubah kembali. Silahkan periksa kembali jawaban kamu</div>
    <button type="submit" class="btn btn-primary">Kirim Jawaban</button>
    {!! Form::close() !!}
  </div>
</div>
@endsection
