<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
  return view('adminlte.login.index');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function(){

  // Home
  Route::get('/home','Admin\HomeController@index');

  // MENU ADMIN
  Route::group(['middleware' => 'role:admin'],function(){
    // User
    Route::resource('/user','Admin\UserController');

    // Siswa
    Route::resource('/siswa','Admin\SiswaController');

    // Mapel
    Route::resource('/mapel','Admin\MapelController');

    // Guru
    Route::resource('/guru','Admin\GuruController');

    // Kelas
    Route::resource('/kelas','Admin\KelasController');
  });

  // MENU GURU
  Route::group(['middleware' => 'role:guru'],function(){
    // Materi
    Route::resource('/materi','Guru\MateriController');
    Route::get('/materi/list-siswa/{id}','Guru\MateriController@listsiswa')->name('materi.listsiswa');
    Route::get('/materi/soal-latihan/{id}','Guru\MateriController@soalLatihan')->name('materi.soal-latihan');
    Route::get('/materi/create-soal-latihan/{id}','Guru\MateriController@createSoal')->name('materi.create-soal-latihan');
    Route::post('/materi/store-soal-latihan/','Guru\MateriController@storeSoal')->name('materi.store-soal-latihan');
    Route::get('/materi/edit-soal-latihan/{id}','Guru\MateriController@editSoal')->name('materi.edit-soal-latihan');
    Route::put('/materi/update-soal-latihan/{id}','Guru\MateriController@updateSoal')->name('materi.update-soal-latihan');
    Route::delete('/materi/hapus-soal-latihan/{id}','Guru\MateriController@soalDestroy')->name('materi.hapus-soal-latihan');
    Route::get('/materi/input-nilai/{materi_id}/{siswa_id}','Guru\MateriController@inputnilai')->name('materi.inputnilai');
    Route::get('/materi/edit-nilai/{materi_id}/{siswa_id}','Guru\MateriController@editnilai')->name('materi.editnilai');
    Route::post('/materi/store/nilai','Guru\MateriController@storenilai')->name('materi.nilai.store');
    Route::put('/materi/update/nilai/{id}','Guru\MateriController@updatenilai')->name('materi.nilai.update');

    // Jawaban
    Route::get('/jawaban','Guru\MateriController@jawaban');

    // DIskusi
    Route::get('/diskusi/guru','Guru\DiskusiController@index')->name('diskusi.guru.index');
    Route::get('/diskusi/guru/create/{id}','Guru\DiskusiController@create')->name('diskusi.guru.create');
    Route::get('/diskusi/guru/open/{id}','Guru\DiskusiController@open')->name('diskusi.guru.open');
    Route::post('/diskusi/guru/store','Guru\DiskusiController@store')->name('diskusi.guru.store');
  });

  // MENU SISWA
  Route::group(['middleware' => 'role:siswa'],function(){
    // Materi
    Route::get('/materiku','Siswa\MateriController@index')->name('materiku');
    Route::post('/materiku/tugas/upload','Siswa\MateriController@uploadtugas')->name('materiku.uploadtugas');
    Route::get('/materiku/tugas/{id}','Siswa\MateriController@tugas')->name('materiku.tugas');
    Route::get('/materiku/masuk/{id}','Siswa\MateriController@masuk')->name('materiku.masuk');
    Route::get('/tugasku','Siswa\MateriController@filetugas')->name('file.tugasku');
    Route::get('/nilaitugasku','Siswa\MateriController@nilai')->name('nilai.tugasku');

    // Soal
    Route::get('/materiku/soal-latihan/{id}','Siswa\MateriController@soalLatihan')->name('materiku.soal-latihan');
    Route::post('/materiku/jawab-soal-latihan/{id}','Siswa\MateriController@jawabSoalLatihan')->name('materiku.jawab-soal-latihan');

    // DIskusi
    Route::get('/diskusi/siswa','Siswa\DiskusiController@index')->name('diskusi.index');
    Route::get('/diskusi/siswa/create/{id}','Siswa\DiskusiController@create')->name('diskusi.create');
    Route::get('/diskusi/siswa/open/{id}','Siswa\DiskusiController@open')->name('diskusi.open');
    Route::post('/diskusi/siswa/store','Siswa\DiskusiController@store')->name('diskusi.store');
  });

});
