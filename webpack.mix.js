let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   //  Admin LTE
   .copyDirectory('resources/assets/adminlte','public/adminlte')
  //  Vendor
   .copyDirectory('resources/assets/vendor/font-awesome','public/vendor/font-awesome')
   .copyDirectory('resources/assets/vendor/summernote','public/vendor/summernote')
   .copyDirectory('resources/assets/vendor/datepicker','public/vendor/datepicker')
   .version();
